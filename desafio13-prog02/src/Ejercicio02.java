import java.util.Random;

public class Ejercicio02 {
    public static void main(String[] args) {
        // crear una matriz 3x3 con elementos generados de forma aleatoria por medio del método random()
        //de la clase Math. Una vez que se genere el array de array mostrar elementos cargados.
        Random azar = new Random();
        int[][] matriz = new int[3][3];
        //Recorre matriz
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matriz[i][j] = azar.nextInt(100);
            }
        }
        //Mostrar elementos cargados
        String line = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                line= line + " " + matriz[i][j];
            }
            line = line + "\n";
        }
        System.out.print("Mostrar elementos de la matriz al azar"+ "\n" + line);
    }
}