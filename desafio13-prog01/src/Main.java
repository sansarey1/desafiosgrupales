public class Main {

    public static void main(String[] args) {
        // Crear un array de 100 elementos que guarde en cada una de las posiciones un número aleatorio entre 1 y 100. El programa debe imprimir en consola todos los valores almacenados en el array. Utiliza un bucle for-each para leer los valores almacenados.
        int i = 0;
        int [] numeros = new int[100];

        numeros[i] = (int)(Math.random()*100);
        for (i= 1; i < 100; i++){
            numeros[i] = (int)(Math.random()*100);
            /*Se debe verificar que los números no se repitan*/
            for(int j= 0; j<i; j++){
                if(numeros[i]==numeros[j]){
                    i--; } }
        }
        for (int elemento:numeros){
            System.out.println(elemento);
        }

    }
}