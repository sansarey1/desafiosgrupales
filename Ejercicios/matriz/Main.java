package matriz;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
	Scanner consola = new Scanner(System.in);
	int matriz[][] = new int[3][3];
	
	for (int x = 0; x < 3; x++){
		  for (int y = 0; y < 3; y++){
		   System.out.println("Introduzca el elemento [" + x + "," + y + "]");
		    matriz[x][y] = consola.nextInt();
		  }
		}
	System.out.println("Matriz original: ");
	for (int x=0; x < matriz.length; x++) {
		  for (int y=0; y < matriz[x].length; y++) {
		    System.out.println ("[" + x + "," + y + "] = " + matriz[x][y]);
		  }
		}
	for(int x = 0; x <3; x++){
	    for(int y = 0; y <3; y++){
	        for(int i= 0; i <3; i++){
	            for(int j = 0; j <3; j++){ 
	             if(matriz[x][y] > matriz [i][j]){
	               int aux = matriz [x][y];
	               matriz [x][y] = matriz [i][j];
	               matriz [i][j] = aux;
	}  
	             }
	        }
	    }
	}
	System.out.println("Matriz ordenada: ");
	for (int x=0; x < matriz.length; x++) {
		  for (int y=0; y < matriz[x].length; y++) {
		    System.out.println ("[" + x + "," + y + "] = " + matriz[x][y]);
		  }
		}
	}

}
