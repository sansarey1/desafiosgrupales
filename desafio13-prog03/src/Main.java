import java.util.Optional;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("*****CARGA DE PERSONAS******");
        System.out.println("¿Cuantas personas desea registrar?");
        Scanner teclado = new Scanner(System.in);
        int qPersonas;
        qPersonas = teclado.nextInt();

        //sumamos 1 a las personas para comenzar luego de los "titulos"
        qPersonas++;

        String matriz[][] = new String[3][qPersonas];
        matriz[0][0]="NOMBRE";
        matriz[1][0]="DNI   ";
        matriz[2][0]="EDAD  ";

        //llamamos al metodo para cargar una matriz
        CMatriz(matriz);
        System.out.println("PERSONAS");
        //llamamos al metodo para mostrar una matriz
        MMatriz(matriz);
        //llamamos al metodo para ordenar la matriz
        OPersonas(matriz);
    }

    //metodo para cargar la matriz
    public static void CMatriz (String matriz[][]) {

        int filas=0, columnas=1;
        String nombre;
        Scanner keyboard = new Scanner(System.in);
        while (columnas < matriz[filas].length) {
            System.out.println("Ingrese nombre de la persona Nº"+columnas+":");
            matriz[filas][columnas] = keyboard.next();
            nombre = matriz[filas][columnas];
            ACompletar(filas, columnas, matriz);
            filas++;
            System.out.println("Ingrese DNI de "+nombre+":");
            matriz[filas][columnas] = keyboard.next();
            ACompletar(filas, columnas, matriz);
            filas++;
            System.out.println("Ingrese la edad de "+nombre+":");
            matriz[filas][columnas] = keyboard.next();
            ACompletar(filas, columnas, matriz);
            filas = 0;
            columnas++;
        }

    }
    //Metodo para mostrar la matriz
    public static void MMatriz(String matriz[][]) {

        for (int filas=0; filas < matriz.length; filas++) {
            System.out.print("|");
            for (int columnas=0; columnas < matriz[filas].length; columnas++) {
                System.out.print (matriz[filas][columnas]);
                System.out.print("|");
                System.out.print("\t");
            }
            System.out.print("\n");
        }
    }

    //Metodo para rellenar los strings hasta la longitud 20 en cada valor del arreglo
    public static void ACompletar(int fila,int columna,String matriz[][]) {
        String caracteres = matriz[fila][columna];
        if(caracteres.length() < 20) {
            int iter = 20-caracteres.length();
            for (int i = 0; i < iter; i++) {
                caracteres=caracteres+" ";
            }
            matriz[fila][columna] = caracteres;
        }
    }

    //Metodo para ordenar los datos de las Personas
    public static void OPersonas(String matriz[][]) {
        int fLimite = matriz.length, cLimite = matriz[0].length, columnas = 1, fila=0, pivot=1;
        String aux;
        if (cLimite == 2) {
            System.out.println("No es necesario ordenar");
        }
        else {
            //definimos un while
            while (pivot != cLimite-1) {
                /*si la columna llega al final, pivot avanza 1 posicion y la columna toma el valor de pivot*/
                if (columnas == cLimite) {
                    pivot++;
                    columnas = pivot;
                }
                /*si el metodo compareTo... devuelve valor positivo, el valor de la cadena es mayor que el valor de la cadena pasada por parámetro*/
                if(matriz[fila][pivot].compareToIgnoreCase(matriz[fila][columnas]) > 0) {
                    aux = matriz[fila][columnas];
                    matriz[fila][columnas] = matriz[fila][pivot];
                    matriz[fila][pivot] = aux;
                    //este while se encarga de ordenar los valores de las filas "dni" y "edad"
                    fila++;
                    while (fila < fLimite ) {
                        aux = matriz[fila][columnas];
                        matriz[fila][columnas] = matriz[fila][pivot];
                        matriz[fila][pivot] = aux;
                        fila++;
                    }
                    fila = 0;
                    columnas++;
                }
                else {
                    columnas++;
                }
            }
            System.out.println("******TABLA DE PERSONAS ORDENADA******");
            MMatriz(matriz);
        }
    }}