package desafiogrupal2;

import java.util.*;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public class Salas {

    private int capacidad;
    private String pelicula;
    private String nombre;
    private Set<Espectadores> espectadores = new HashSet<Espectadores>();

    public Salas(int capacidad, String nombre) {
        this.capacidad = capacidad;
        this.nombre = nombre;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public boolean setEspectadores(Espectadores espectador) {
        if (capacidad > espectadores.size()) {
            espectadores.add(new Espectadores(nombre, capacidad, pelicula, capacidad));
            return true;
        } else {
            return false;
        }
    }

    public void getEspectadores() {
        Iterator<Espectadores> iterador = espectadores.iterator();
        if (espectadores.size() > 0) {
            System.out.println("El total de espectadores es de : " + espectadores.size());
            while (iterador.hasNext()) {
                System.out.println("Espectador: " + iterador.next().getNombre() + "En la butaca numero #" + iterador.next().getButaca() + "\n");
            }
        }else{
            System.out.println("SIN ESPECTADORES CARGADOS");
        }
    }

    @Override
    public String toString() {
        return "Salas{" + "capacidad=" + capacidad + ", pelicula=" + pelicula + ", nombre=" + nombre + ", espectadores=" + espectadores + '}';
    }

}
