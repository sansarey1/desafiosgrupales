package desafiogrupal2;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public class Acomodadores extends Personas implements ParaAcomodadores{
    
    private Salas sala;
    private double sueldo;
    public Acomodadores(String nombre, int edad){
        super.setEdad(edad);
        super.setNombre(nombre);
    }
    public void setSueldo(double sueldo){
        this.sueldo = sueldo;
    }
    @Override
    public Salas getSala() {
        return sala;
    }

    @Override
    public void setSala(Salas sala) {
        this.sala = sala;
    }

    @Override
    public String getTipo(Personas persona) {
        return ("Acomodador");
    }
    
    @Override
    public String mostrarDatos(Personas persona) {
        return "Nombre: "+super.getNombre()+"\nEdad: "+super.getEdad()+"\nSala: "+sala+"\nTipo: "+getTipo(persona);
    }
}
