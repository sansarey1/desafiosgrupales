package desafiogrupal2;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public abstract class Personas {
    private String nombre;
    private int edad;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public abstract String getTipo(Personas persona);
    
    public abstract String mostrarDatos(Personas persona);

    @Override
    public String toString() {
        return "Personas{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }
    
}
