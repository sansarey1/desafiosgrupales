package desafiogrupal2;
import java.util.Scanner;
public class Cine {
    public static void main(String[] args) {
        //1.	Una sala de cine debe contener la capacidad de la sala, 
        //la película que se proyecta en la misma, el nombre de la sala 
        //y el listado de espectadores asignados a la misma.
        Scanner leer = new Scanner(System.in);
        //Cargar espectadores pidiendo los nombre, edad, fila y silla al
        //
        int seguir = 1;

        try {
            Espectadores espectadores1 = new Espectadores("Uli", 19, "V", 1);
            Espectadores espectadores2 = new Espectadores("Flor", 27, "V", 2);
            Salas sala01 = new Salas(300, "Sala chingona");
            sala01.setPelicula("Rapidos y Furiosos XXV");
            sala01.setEspectadores(espectadores1);
            sala01.setEspectadores(espectadores2);

            System.out.println(sala01.setEspectadores(espectadores1));
            System.out.println(sala01.setEspectadores(espectadores2));

            Acomodadores acomodador01 = new Acomodadores("Franco Perez", 30);
            acomodador01.setSala(sala01);
            acomodador01.setSueldo(50000);

            System.out.println(acomodador01.toString());

            Empleados empleado1 = new Empleados("Roberto Flores", 27);
            System.out.println(empleado1.toString());


        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("ERROR EN EL INGRESO DE DATOS");
        }
        while (seguir == 1) {
                /*
                11.1) Cargar espectadores pidiendo los nombre, edad, fila y silla al
                usuario por medio de la consola. En caso de que el usuario
                ingrese letras en lugar de números donde no corresponda el
                programa debe finalizar con el error: "ERROR EN EL INGRESO DE DATOS".
                 */

            System.out.println("NUEVO ESPECTADOR");

            System.out.print("Nombre: ");
            String nombre = leer.next();

            System.out.print("Edad: ");
            int edad = leer.nextInt();

            System.out.print("Fila: ");
            String fila = leer.next();

            System.out.print("Silla: ");
            int silla = leer.nextInt();

            System.out.println("Para cargar otra espectador presione 1, de lo contrario presiones 0.");
            seguir = leer.nextInt();

            Espectadores.add(new Espectadores(nombre, edad, fila, silla));

        }
        /*2.	Las Salas no deben permitir la asignación de una lista de
        Espectadores superior a su capacidad*/
        //Método setEspectadores en Salas
            
            
        /*3.	Se debe poder listar los espectadores de una sala, pero en
        caso de que no haya sido asignada se debe capturar el error emitiendo 
        el mensaje "SIN ESPECTADORES CARGADOS".*/
        //Método getEspectadores en clase Salas
            
            
        /*4.	El Programa debe por lo menos una clase abstracta Personas de 
        la cual hereden Espectadores y Empleados:*/
        //Done
        /*5.	La clase Personas deberá tener por lo menos dos método abstractos: 
        a.	Uno que permitirá ver si la persona es un Espectador, un Acomodador 
            o un Empleado dependiendo de qué clase sea.
        b.	Uno que permitirá visualizar todos los datos de la persona.*/
        //Done
        /*6.	Los Espectadores ademá de sus datos personales deben estar asignados 
            a una butaca definida por la Fila (una letra) y la Silla (un entero entero). */
        /*7.	La clase Empleados deberá contener el Sueldo que cobra el empleado y permitir 
        su posterior modificación. */
        /*8.	El programa contendrá una clase Acomodadores que heredará de la clase Empleados
        e implementará la interfaz ParaAcomodadores. */
        /*9.	A los Acomodadores se les podrá designar una sala o modificar la sala asignada.*/
        /*10.	La interfaz ParaAcomodadores deberá permitir la asignación y modificación de una
        sala a un acomodador. */
        /*11.	El programa debe tener una clase principal llamada Cine en donde se deben realizar las siguientes operaciones:
a.	Cargar espectadores pidiendo los nombre, edad, fila y silla al usuario por medio de la consola. En caso de que el usuario ingrese letras en lugar de números donde no corresponda el programa debe finalizar con el error: "ERROR EN EL INGRESO DE DATOS". 
b.	Por medio del código crear una sala con capacidad para 3 personas llamada “Sala 01” en donde se proyecte la película “Jocker”.
c.	Asignar a la sala los espectadores cargados. 
d.	Imprimir la lista de espectadores que se encuentran asignado a la sala. 
e.	Crear un acomodador por medio del código.
f.	Asignar a dicho Acomodador la sala creada anteriormente. 
g.	Asignar un sueldo de $50.000 al Acomodador.
h.	Mostrar los datos del Acomodador en la consola.
i.	Crear un Empleado. 
j.	Mostrar los datos del Empleado.
*/
        /*12.	Todas las clases instanciables del programa deben tener los métodos geters, 
        seters para ingresar o visualizar datos necesarios para los procesos solicitados y el
        método toString que mostrará todos los datos relevantes de la clase. */


    }
}