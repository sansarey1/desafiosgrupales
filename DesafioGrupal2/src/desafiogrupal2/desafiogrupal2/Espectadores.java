package desafiogrupal2;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public class Espectadores extends Personas {
    
    private String fila;
    private int silla;
    
    public Espectadores(String nombre, int edad, String fila, int silla){
        super.setNombre(nombre);
        super.setEdad(edad);
        this.fila = fila;
        this.silla = silla;
    }

    public static void add(Espectadores espectadores) {
    }

    public String getButaca(){
        return String.valueOf(silla);
    }

    @Override
    public String getTipo(Personas persona) {
        return ("Espectador");
    }
    @Override
    public String mostrarDatos (Personas persona){
        return "Nombre: "+ super.getNombre()+ "\nEdad: "+super.getEdad()+ "\nSilla: "+silla+ "\nfila: "+fila+ "\nTipo: "+getTipo(persona);
    }
}
