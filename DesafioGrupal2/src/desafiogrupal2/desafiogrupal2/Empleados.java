package desafiogrupal2;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public class Empleados extends Personas{
    private double sueldo;
    
    public Empleados(String nombre, int edad){
        super.setNombre(nombre);
        super.setEdad(edad);
    }
    public void setSueldo(double sueldo){
        this.sueldo = sueldo;
    }

    @Override
    public String getTipo(Personas persona) {
        return ("Empleado");
    }

    @Override
    public String mostrarDatos(Personas persona) {
    return  "Nombre:" + super . getNombre () + "\nEdad:" + super . getEdad () + "\nSueldo:" + sueldo + "\nTipo:" + getTipo ( persona ) ;
}

}
