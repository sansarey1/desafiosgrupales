package desafiogrupal2;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public interface ParaAcomodadores {
    public Salas getSala();
    public void setSala(Salas sala);
}
