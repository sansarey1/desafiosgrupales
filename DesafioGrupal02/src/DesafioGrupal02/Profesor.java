package DesafioGrupal02;
// Las clases Estudiantes y Profesores descienden de la clase abstracta Personas.

public class Profesor extends Personas{

    private double basico;
    private int antiguedad;

    public Profesor(double basico, int antiguedad, String nombre, String apellido, int legajo) {
        super.setApellido(apellido);
        super.setLegajo(legajo);
        super.setNombre(nombre);
        this.basico = basico;
        this.antiguedad = antiguedad;
    }

    // El metodo calcularSueldo() de la clase Profesores calcula el sueldo sumando un 10%  al basico por cada año de antiguedad.


    public double calcularSueldo(){
        double sueldo = (((10*this.basico)/100)*this.antiguedad)+this.basico;
        return sueldo;
    }

    @Override
    public void modificarDatos() {

    }

    //Getters y Setters


    public double getBasico() {
        return basico;
    }

    public void setBasico(double basico) {
        this.basico = basico;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    @Override
    public String toString() {
        return "\n \n \t\t\t Profesor [ Nombre:"+super.getNombre()+" "+super.getApellido()+" Legajo: "+super.getLegajo()+" basico=" + basico + ", antiguedad=" + antiguedad + "]";
    }



}
