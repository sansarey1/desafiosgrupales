package DesafioGrupal02;

/*
 * 10) Las clases Estudiantes y Profesores descienden de la clase abstracta Personas.
 */

public class Estudiante extends Personas{

    public Estudiante(String nombre, String apellido, int legajo){
        super.setNombre(nombre);
        super.setApellido(apellido);
        super.setLegajo(legajo);
    }

    @Override
    public void modificarDatos() {
        // TODO Auto-generated method stub

    }

    @Override
    public String toString() {
        return "\n \t\t\t\t Estudiante [Nombre()=" + getNombre() + ", Apellido()=" + getApellido() + ", Legajo()="
                + getLegajo() + "\n ]";
    }



}
