package DesafioGrupal02;

import java.util.HashSet;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Facultad implements Informacion {

    private String nomFac;
    private Set<Carrera> carreras = new HashSet<Carrera>();

    public Facultad(String nombre) {
        this.nomFac = nombre;
    }

    public void agregarCarrera(String nombre) {
        carreras.add(new Carrera(nombre));
    }

    public void eliminarCarrera(String nombre) {

        Iterator<Carrera> eliminar = carreras.iterator();

        while (eliminar.hasNext()) {
            if (eliminar.next().getNombre().equals(nombre)) {
                eliminar.remove();
                System.out.println(nombre + " fue eliminada");
            }
        }

    }

    public void agregarMateriaCarrera(String nombreCarrera, String nombreMateria, Profesor titular) {

        boolean encontroCarrera = false;

        for (Carrera carrera : carreras) {
            if (carrera.getNombre().equals(nombreCarrera)) {
                carrera.agregarMateria(nombreMateria, titular);
                System.out.println("Se agreg� " + nombreMateria + " a " + carrera.getNombre() + " de forma exitosa.");
                encontroCarrera = true;
                break;
            }
            if (!encontroCarrera) {
                System.out.println("No existe la carrera");
            }
        }

    }

    public void eliminarMateriaDeCarrera(String nombreCarrera, String nombreMateria) {
        for (Carrera carrera : this.carreras) {
            carrera.eliminarMateria(nombreMateria);
        }
    }

    public void encontrarMateriaEnCarrera(String nombreCarrera, String nombreMateria) {

        for (Carrera carrera : this.carreras) {
            if (carrera.getNombre().equals(nombreCarrera)) {
                System.out.println("carrera encontrada");
                if (carrera.encontrarMateria(nombreMateria) != null) {
                    System.out.println("materia encontrada. �Quiere eliminarla?");
                    Scanner lectura = new Scanner(System.in);

                    // Todos los lugares que signifiquen el ingreso de datos deberan establecer controles por posibles ingresos erroneos de datos.

                    try {
                        String opcion = lectura.next();

                        if(opcion.equals("si")) {
                            this.eliminarMateriaDeCarrera(nombreCarrera, nombreMateria);
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                        System.out.println("todo mal!!!");
                    }
                }
            }
        }

    }

    public void agregarEstudiante(String nombreCarrera, String nombreMateria, Estudiante estudiante) {
        for (Carrera carrera : this.carreras) {
            if (carrera.getNombre().equals(nombreCarrera)) {
                System.out.println("carrera encontrada");
                if (carrera.encontrarMateria(nombreMateria) != null) {
                    carrera.encontrarMateria(nombreMateria).agregarEstudiante(estudiante.getNombre(), estudiante.getApellido(), estudiante.getLegajo());
                }
            }
        }
    }

    public void eliminarEstudiante(String nombreCarrera, String nombreMateria, int nroLegajo) {
        for (Carrera carrera : this.carreras) {
            if (carrera.getNombre().equals(nombreCarrera)) {
                System.out.println("carrera encontrada");
                if (carrera.encontrarMateria(nombreMateria) != null) {
                    carrera.encontrarMateria(nombreMateria).eliminarEstudiante(nroLegajo);
                }
            }
        }
    }

    public void modificarTitular(String nombreCarrera, String nombreMateria, Profesor profesorNuevo){
        for (Carrera carrera : this.carreras) {
            if (carrera.getNombre().equals(nombreCarrera)) {
                if (carrera.encontrarMateria(nombreMateria) != null) {
                    carrera.encontrarMateria(nombreMateria).setTitular(profesorNuevo);
                }
            }
        }
    }

    public String getNomFac() {
        return nomFac;
    }

    public void setNomFac(String nomFac) {
        this.nomFac = nomFac;
    }

    public TreeSet<Carrera> getCarreras() {
        return (TreeSet<Carrera>) carreras;
    }

    public void setCarreras(TreeSet<Carrera> carreras) {
        this.carreras = carreras;
    }

    @Override
    public int verCantidad() {
        // TODO Auto-generated method stub
        return this.carreras.size();
    }

    // El metodo listarContenidos() de la interface Informacion lista los elementos de la clase contenida, es decir que de la clase Facultad se listan las Carreras, de la clase Carreras las materias, etc.


    @Override
    public String listarContenidos() {
        // TODO Auto-generated method stub
        return this.carreras.toString();
    }

    @Override
    public String toString() {
        return "\n Facultad [nomFac=" + nomFac + ", carreras=" + carreras + "]";
    }

}
























