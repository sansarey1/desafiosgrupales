package DesafioGrupal02;

class Principal {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        Facultad frr = new Facultad("UTN-FRR");

        // Agregar Carreras a una Facultad

        frr.agregarCarrera("IEM");
        frr.agregarCarrera("MED");
        frr.agregarCarrera("LAC");

        // Eliminar Carreras de la facultad


        frr.eliminarCarrera("IEM");

        //Eliminar Estudiantes de una facultad implica que se elimine el estudiante de cada una de las materias a las cuales se inscribio.



        //Agregar Profesor a una Materia.


        Profesor titular = new Profesor(15000, 2, "Facundo", "Uferer", 3553);

        //Agregar materias a una carrera


        frr.agregarMateriaCarrera("MED", "Anatomia", titular);

        frr.agregarMateriaCarrera("LAC", "Barroco 1 ", titular);

        frr.agregarMateriaCarrera("LAC", "Renacimiento 2", titular);

        //Eliminar materias de una carrera

        frr.eliminarMateriaDeCarrera("LAC", "Renacimiento 2");

        //Encontrar materias de una carrera en particular indicando el nombre de la materia. Si la materia existe nos debera preguntar si deseamos eliminar.

        frr.encontrarMateriaEnCarrera("MED", "Anatomia");

        //Agregar Estudiantes a una Materia.


        Estudiante estudiante = new Estudiante("Raul","Marquez" , 2588);
        Estudiante estudiante2 = new Estudiante("Mara","Fernandez" , 4585);

        frr.agregarEstudiante("MED", "Anatomia", estudiante);
        frr.agregarEstudiante("MED", "Anatomia", estudiante2);

        //Eliminar estudiantes de una materia

        frr.eliminarEstudiante("MED", "Anatomia", 2548);

        // 9) Modificar el Profesor de la materia


        Profesor nuevoProvesor = new Profesor(16818, 2, "Javier", "Santos", 841);

        frr.modificarTitular("TSP", "Laboratorio de Computaci�n II", nuevoProvesor);

        System.out.println(frr.toString());

    }

}

